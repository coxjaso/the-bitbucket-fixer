function addIgnoreWhiteSpaceToLinks() {
	console.log("whitespace adder");
	$('a[data-qa="pull-request-row-link"]').each(function() {
		let link = $(this).prop('href');
		if (link.indexOf("w=1") === -1) {
			link += "?w=1";
			$(this).prop('href', link);
		}
	});
}
addIgnoreWhiteSpaceToLinks();