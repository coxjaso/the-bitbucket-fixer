
function nthIndex(str, pat, n){
	var L= str.length, i= -1;
	while(n-- && i++<L){
		i= str.indexOf(pat, i);
		if (i < 0) break;
	}
	return i;
}

function addFeatureToInput() {
	console.log("add to input");
	let currentVal = $('#id_branch_name').val();
	if (!currentVal.includes("feature/")) {
		let newValue = "feature/" + currentVal.substring(0, nthIndex(currentVal,'-',2));
		$('#id_branch_name').val(newValue);
		$('.target .name').html(newValue)
	}
}
addFeatureToInput();