# The BitBucket Fixer!

The idea is simple: The BitBucket Team refuses to fix the obvious.  So we did...
  - Tab size is 4, like most IDE's out there.
  - Added code syntax highlighting.
  - Added a "Reviewed" button and a toggle button.

### Needed Features!

  1. Someone with more time than me might find a better way to apply syntax highlighting.  Right now I put it in a timeout to look for "pre" blocks that don't have the highlight class.  With all the ajax stuff going on on bitbucket, this was the cop-out.
  2. Tab-size should probably be a variable.

# 

#### Thanks for the help!

Jason D. Cox [LinkedIn](https://www.linkedin.com/in/coxjaso)
Earl Swigert [LinkedIn](https://www.linkedin.com/in/earl-swigert-ba42521b/)




License
----
MIT - Use it, fix it, steal it, have fun.
