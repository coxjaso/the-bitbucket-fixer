
function addToggleButton() {
	window.setTimeout(function() {
		let elements = document.getElementsByClassName("commentable-diff");

		if (!elements || elements.length == 0) {
			console.log("Nothing there, wait your turn!")
			return addToggleButton();
		}

		for (let i = 0; i < elements.length; i++) {
			let element = elements[i];
			let sourceTag = element.getAttribute('data-source-url');
			let path = element.getAttribute('data-path');
			let diffBlock = elements[i].getElementsByClassName("diff-content-container")[0];

			let collapseKey = md5(diffBlock.innerText)
			let reviewedKey = collapseKey + "_done";

			if (element.getElementsByClassName('bb-fixer').length == 0) {
				/**
				 * Button for show/hide
				 * @type {HTMLElement}
				 */
				let button = document.createElement('button');
				button.innerHTML = "Toggle Collapse";
				button.classList.add('bb-fixer', 'aui-button', 'aui-button-light', 'sbs');
				button.onclick = function() {
					diffBlock.style.display = diffBlock.style.display === 'none' ? '' : 'none';

					chrome.storage.sync.set({[collapseKey]: diffBlock.style.display === 'none'}, function() {
						console.log("Set!");
					});
				}

				chrome.storage.sync.get([collapseKey], function(result) {
					if (result[collapseKey]) {
						diffBlock.style.display = result[collapseKey] ? 'none' : '';
					}
				});

				/**
				 * Button for Reviewed/not reviewed.
				 * @type {HTMLElement}
				 */
				let reviewedHtml = `<span class="badge approved">&nbsp;&nbsp;&nbsp;&nbsp;</span> Reviewed`;
				let reviewedButton = document.createElement('button');

				reviewedButton.innerHTML = "Reviewed";
				reviewedButton.classList.add('bb-fixer', 'aui-button', 'aui-button-light', 'sbs');
				reviewedButton.onclick = function() {
					reviewedButton.classList.toggle("done");
					if (reviewedButton.classList.contains('done')) {
						$('li[data-file-identifier="'+path+'"').addClass("file-done");
						diffBlock.style.display = 'none';
						reviewedButton.innerHTML = reviewedHtml;
					}
					else {
						$('li[data-file-identifier="'+path+'"').removeClass("file-done");
						diffBlock.style.display = '';
						reviewedButton.innerHTML = "Reviewed";
					}

					chrome.storage.sync.set({
						[reviewedKey]: reviewedButton.classList.contains("done"),
						[collapseKey]: reviewedButton.classList.contains("done")
					}, function() {
						console.log("Set!");
					});
				}

				chrome.storage.sync.get([reviewedKey], function(result) {
					if (result[reviewedKey]) {
						reviewedButton.classList.add('done')
						reviewedButton.innerHTML = reviewedHtml;
						$('li[data-file-identifier="'+path+'"').addClass("file-done");
					}
				});

				let headerSpot = element.getElementsByClassName('heading')[0]
					.getElementsByClassName('primary')[0]
					.getElementsByClassName('filename')[0];

				headerSpot.prepend(button);
				headerSpot.prepend(reviewedButton);
			}
		}
	}, 500)
}

function toggleTimeout() {
	window.setTimeout(function() {
		let elements = document.getElementsByClassName("commentable-diff");

		if (!elements || elements.length == 0) {
			console.log("Nothing there, wait your turn!")
			return toggleTimeout();
		}
		else {
			return addToggleButton();
		}
	}, 1000)
}


function checkDOMChange()
{
	// check for any new element being inserted here,
	// or a particular node being modified
	let elements = $('pre:not(.hljs)');

	$.each(elements, function (i, elem) {
		// Adding code comment.
		if ($(elem).closest(".new-comment")[0] !== undefined) {
			return;
		}

		let parent = $(elem).closest('section');
		let fileName = $(parent).data().path;

		if (fileName) {
			let re = /(?:\.([^.]+))?$/;
			let ext = re.exec(fileName)[1];

			if (ext == "scss") {
				ext = "css";
			}
			$(elem).addClass(ext);
		}
		hljs.highlightBlock(elem);
	})

	// call the function again after 100 milliseconds
	setTimeout( checkDOMChange, 2000 );
}



chrome.storage.sync.get(['navLeft', 'tabSize', "autoW1", "disableExtension"], function(result) {
	if (result.disableExtension != 1) {
		toggleTimeout();
		checkDOMChange();
	}
	else {
		return;
	}

	if (result.navLeft) {
		document.getElementsByTagName('body')[0].classList.add('bb-fixer-left');

		let initialScrollPostion;
		let scrollCheck = function() {
			let navElement = $('.bb-fixer-left #pullrequest-diff > .main:first-child')

			if (!navElement) {
				return false;
			}

			if (!initialScrollPostion) {
				initialScrollPostion = navElement.offset().top;
			}

			let scrollPoint = document.documentElement.scrollTop;

			if (initialScrollPostion < scrollPoint) {
				navElement.addClass('fixed');
			}
			else {
				navElement.removeClass('fixed');
			}

			let navLinks = document.getElementsByClassName("commit-files-summary--filename");
			for (let i = 0; i < navLinks.length; i++) {
				if (navLinks[i].innerText.length > 60) {
					navLinks[i].innerText = '...' + navLinks[i].innerText.substr(length - 60);
				}
			}
		}

		$(document).scroll(scrollCheck);
	}

	let tabCSS = `pre { tab-size: ${result.tabSize || 4}; }`;
	let head = document.head || document.getElementsByTagName('head')[0];
	let tabStyle = document.createElement('style');
	tabStyle.type = 'text/css';
	tabStyle.appendChild(document.createTextNode(tabCSS));
	head.appendChild(tabStyle);

	let regex = /pull-requests\/[0-9]/g;
	if (window.location.href.match(regex) && result.autoW1) {
		let location = window.location.href;

		if (location.includes("w=1")) {
			let button = $('<a class="aui-button" href="' + location.replace("w=1", "w=0") + '"><span>Include Whitespace</span></a>');
			$('#pullrequest-actions').prepend(button)

			return;
		}
		else if (location.includes("w=0")) {
			let button = $('<a class="aui-button" href="' + location.replace("w=0", "w=1") + '"><span>Ignore Whitespace</span></a>');
			$('#pullrequest-actions').prepend(button)

			return;
		}
		else {
			if (location.includes("?")) {
				location += '&w=1';
			}
			else {
				location += "?w=1";
			}

			window.location = location;
		}
	}
});

