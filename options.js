// Copyright 2018 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

'use strict';

let settingsList = ['navLeft', 'tabSize', 'autoW1', 'disableExtension'];

document.getElementById("clear").addEventListener('click', function(e) {
	chrome.storage.sync.get(settingsList, function(result) {
		chrome.storage.sync.clear(function() {
			// Reset important settings
			chrome.storage.sync.set(result, function() {
				// Refresh screen
				chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
					chrome.tabs.update(tabs[0].id, {url: tabs[0].url});
				});
			});
		});
	});

});

document.getElementById("nav-left").addEventListener('click', function(e) {
	chrome.storage.sync.set({navLeft: true}, function() {
		console.log("Set")
		chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
			chrome.tabs.update(tabs[0].id, {url: tabs[0].url});
		});
	});
});

document.getElementById("nav-top").addEventListener('click', function(e) {
	chrome.storage.sync.set({navLeft: false}, function() {
		console.log("Set")
		chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
			chrome.tabs.update(tabs[0].id, {url: tabs[0].url});
		});
	});
});

document.getElementById("tab-size").addEventListener('change', function(e) {
	var newValue = e.target.value;
	chrome.storage.sync.set({tabSize: newValue}, function() {
		chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
			chrome.tabs.update(tabs[0].id, {url: tabs[0].url});
		});
	});
});

document.getElementById("auto-w1").addEventListener('change', function(e) {
	var newValue = e.target.checked;
	chrome.storage.sync.set({autoW1: newValue}, function() {

	});
});

document.getElementById("disableExtension").addEventListener('change', function(e) {
	var newValue = e.target.value;
	chrome.storage.sync.set({disableExtension: newValue}, function() {
		chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
			chrome.tabs.update(tabs[0].id, {url: tabs[0].url});
		});
	});
});


chrome.storage.sync.get(['navLeft', 'tabSize', "autoW1", "disableExtension"], function(result) {
	if (result.navLeft) {
		document.getElementById("nav-left").checked = true;
	}
	else {
		document.getElementById("nav-top").checked = true;
	}

	document.getElementById("tab-size").value = result.tabSize || 4;

	document.getElementById("auto-w1").checked = result.autoW1;

	document.getElementById("disableExtension").value = result.disableExtension || 0;

});
